package com.example.demo.controller;
import org.springframework.web.bind.annotation.*;
import java.time.Instant;



@RestController
public class HelloWorld {

   @GetMapping("/")
    Mono<String> welcome() {
         return Mono.just("Hello World!"); // возврат значения
    }
    
    @RequestMapping("/time")
    Instant time(){
        return Instant.now();
    }

}
